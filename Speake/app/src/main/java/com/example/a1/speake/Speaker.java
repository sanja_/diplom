package com.example.a1.speake;

import android.content.Context;
import android.media.AudioManager;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import java.util.HashMap;
import java.util.Locale;

public class Speaker implements TextToSpeech.OnInitListener {
    private static final String TAG = "myLogs";
        private TextToSpeech tts;

        private boolean ready = false;

        private boolean allowed = false;

        public Speaker(Context context){
                tts = new TextToSpeech(context, this);
        }

        public boolean isAllowed(){
                return allowed;
        }

        public void allow(boolean allowed){
                this.allowed = allowed;
        }

        @Override
        public void onInit(int status) {
                if(status == TextToSpeech.SUCCESS){
                        // Change this to match your
                        // locale
                        Locale locale=new Locale("ru");
                        tts.setLanguage(locale);
                        ready = true;
                }else{
                        ready = false;
                }
        }

        public void speak(String text){

                // Speak only if the TTS is ready
                // and the user has allowed speech
            Log.d(TAG,"5555555555555555555555555555555555555555555555");
                if(ready) {
                        HashMap<String, String> hash = new HashMap<String,String>();
                        hash.put(TextToSpeech.Engine.KEY_PARAM_STREAM,
                                String.valueOf(AudioManager.STREAM_NOTIFICATION));
                    Log.d(TAG,"66666666666666666666666666666666666666666"+hash);
                        tts.speak(text, TextToSpeech.QUEUE_ADD, hash);
                }
        }

        public void pause(int duration){
                tts.playSilence(duration, TextToSpeech.QUEUE_ADD, null);
        }

        // Free up resources
        public void destroy(){
                tts.shutdown();
        }





}

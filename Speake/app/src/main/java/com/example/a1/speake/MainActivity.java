package com.example.a1.speake;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.speech.tts.TextToSpeech;
import android.support.v4.util.ArrayMap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ToggleButton;
import android.util.Log;

import java.util.Locale;

import static com.example.a1.speake.IncomeMessage.Sender;

public class MainActivity extends Activity {


   // static ArrayMap<String, String> Sender=new ArrayMap<>();

    private static final String TAG = "myLogs";

    private Button mButton;
   // private TextToSpeech mTTS;

    private final int CHECK_CODE = 0x1;
    private final int LONG_DURATION = 5000;
    private final int SHORT_DURATION = 1200;

    private Speaker speaker;
    public IncomeMessage incMessage;


    private ToggleButton toggle;
    private CompoundButton.OnCheckedChangeListener toggleListener;

    private BroadcastReceiver smsReceiver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

       // getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);

        toggle = (ToggleButton)findViewById(R.id.speechToggle);


        toggleListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton view, boolean isChecked) {
                if(isChecked){
                    speaker.allow(true);
                    speaker.speak(getString(R.string.start_speaking));
                   // sp();
                    speaker.speak(smsReceiver.getResultData());
                    Log.d(TAG,"sssssssssssssssssssssssssssssssssssssssssssssssssss"+smsReceiver.getResultData());

                }else{
                    speaker.speak(getString(R.string.stop_speaking));
                    speaker.allow(false);
                }
            }
        };
        toggle.setOnCheckedChangeListener(toggleListener);

        checkTTS();
        initializeSMSReceiver();
        registerSMSReceiver();
    }


    private void registerSMSReceiver() {
        IntentFilter intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        registerReceiver(smsReceiver, intentFilter);
    }


    private String getContactName(String phone){
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phone));
        String projection[] = new String[]{ContactsContract.Data.DISPLAY_NAME};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if(cursor.moveToFirst()){
            return cursor.getString(0);
        }else {
            return phone;
        }
    }


    private ArrayMap<String, String> initializeSMSReceiver(){
        smsReceiver = new BroadcastReceiver(){
            @Override
            public void onReceive(Context context, Intent intent) {

                Bundle bundle = intent.getExtras();
                ArrayMap<String, String> Sender=new ArrayMap<>();
                String text = null;
                String sender;
                if(bundle!=null){

                    Object[] pdus = (Object[])bundle.get("pdus");
                    for(int i=0;i<pdus.length;i++){
                        byte[] pdu = (byte[])pdus[i];
                        SmsMessage message = SmsMessage.createFromPdu(pdu);


                        text = message.getDisplayMessageBody();
                        sender = getContactName(message.getOriginatingAddress());

                      //  Sender.put(sender,text);

                        speaker.pause(LONG_DURATION);
                        speaker.speak("Пришло Сообщение от абонента"+sender+ "!");
                        Log.d(TAG,"eeeeeeeeeeeeeeeeeeeeeeeeee"+sender);


                       // speaker.pause(SHORT_DURATION);
                       // speaker.speak(text);
                        //smsSender.setText("Message from " + sender);
                       // smsText.setText(text);

                    }
                    setResultData(text);
                    Log.d(TAG,"eeeeeeeeeeeeeeeeeeeeeeeeee"+getResultData());
                }

            }
            //return Sender;

        };
       // Log.d(TAG,"eeeeeeeeeeeeeeeeeeeeeeeeee"+Sender);

        Log.d(TAG,"eeeeeeeeeeeeeeeeeeeeeeeeee"+Sender);
        return Sender;
    }

    public void sp(){
        String SenderMessage;
       // ArrayMap<String, String> Sende=Sender;
        ///for(int i=0;i<Sender.size();i++){
        //    SenderMessage=Sender.keyAt(i);
         //   Log.d(TAG,"111111111111111111111111111111111111111111111111111"+SenderMessage);
            speaker.pause(LONG_DURATION);
        speaker.speak("ку работяги");
            //speaker.speak("You have a new message from" + SenderMessage + "!");
          //  speaker.pause(SHORT_DURATION);
          //  speaker.speak(Sender.get(SenderMessage));
      //  }
       // speaker.speak("Пришло Сообщение!");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == CHECK_CODE){
            if(resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS){
            }else {
                Intent install = new Intent();
                install.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(install);
            }
        }
    }

    private void checkTTS(){
        Intent check = new Intent();
        check.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(check, CHECK_CODE);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(smsReceiver);
        speaker.destroy();
    }

 /*
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toggle = (ToggleButton)findViewById(R.id.speechToggle);

        checkTTS();
        initializeSMSReceiver();
        registerSMSReceiver();

        toggleListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton view, boolean isChecked) {
                if(isChecked){
                  //  speaker.allow(true);
                    speaker.speak(getString(R.string.start_speaking));
                    Log.d(TAG,"daaaaaaaaaaaaaaaaaaaaaaaaaa"+initializeSMSReceiver());
                    Spech(initializeSMSReceiver());
                }else{
                    speaker.speak(getString(R.string.stop_speaking));
                    //speaker.allow(false);
                }
            }
        };
        toggle.setOnCheckedChangeListener(toggleListener);

    }






    public void Spech(ArrayMap<String, String> Sender){
        String SenderMessage;
        ArrayMap<String, String> Sende=Sender;
        for(int i=0;i<Sende.size();i++){
            SenderMessage=Sende.keyAt(i);
            Log.d(TAG,"111111111111111111111111111111111111111111111111111"+SenderMessage);
            speaker.pause(LONG_DURATION);
            speaker.speak("You have a new message from" + SenderMessage + "!");
            speaker.pause(SHORT_DURATION);
            speaker.speak(Sende.get(SenderMessage));
        }
    }

    private void checkTTS(){
        Intent check = new Intent();
        check.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(check, CHECK_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == CHECK_CODE){
            if(resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS){
                speaker = new Speaker(this);
            }else {
                Intent install = new Intent();
                install.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(install);
            }
        }
    }

    public ArrayMap<String, String> initializeSMSReceiver() {
        smsReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    Object[] pdus = (Object[]) bundle.get("pdus");

                    ArrayMap<String, String> Sender=incMessage.isWriteIncomeMessages(pdus);

                }
            }

        };
        return Sender;
    }


    public void registerSMSReceiver() {
        IntentFilter intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        registerReceiver(smsReceiver, intentFilter);
    }
*/
}


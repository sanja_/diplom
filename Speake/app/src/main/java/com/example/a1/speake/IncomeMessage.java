package com.example.a1.speake;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.SmsMessage;

import android.util.Log;
import android.support.v4.util.ArrayMap;



public class IncomeMessage extends Activity {
    static ArrayMap<String, String> Sender=new ArrayMap<>();
    private static final String TAG = "myLogs";
    private Speaker speaker;
    private final int CHECK_CODE = 0x1;
    private final int LONG_DURATION = 5000;
    private final int SHORT_DURATION = 1200;
    private BroadcastReceiver smsReceiver;

   /* public ArrayMap<String, String> initializeSMSReceiver() {
        smsReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    isWriteIncomeMessages(pdus);
                    ArrayMap<String, String> Sender=isWriteIncomeMessages(pdus);
                }
            }
        };
        return Sender;
    }*/

    public ArrayMap<String, String> isWriteIncomeMessages(Object[] pdus){
        ArrayMap<String, String> Sender=new ArrayMap<>();
        for(int i=0;i<pdus.length;i++){
            byte[] pdu = (byte[])pdus[i];
            SmsMessage message = SmsMessage.createFromPdu(pdu);
            Sender.put(getContactName(message.getOriginatingAddress()),message.getDisplayMessageBody());
            Log.d(TAG,"4444444444444444444444444444444444444444444444444444"+Sender);
          //  String text = message.getDisplayMessageBody();
          //  String sender = getContactName(message.getOriginatingAddress());

            //  smsSender.setText("Message from " + sender);
            //  smsText.setText(text);
        }
    return Sender;
    }




    private String getContactName(String phone){
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phone));
        String projection[] = new String[]{ContactsContract.Data.DISPLAY_NAME};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if(cursor.moveToFirst()){
            return cursor.getString(0);
        }else {
            return "unknown number";
        }
    }




}

/* ====================================================================
 * Copyright (c) 2014 Alpha Cephei Inc.  All rights reserved.
 * ====================================================================
 */

package edu.cmu.pocketsphinx.demo;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.res.AssetManager;
import android.database.Cursor;

import android.media.SoundPool;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.speech.tts.TextToSpeech;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.ArrayMap;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPropertyAnimatorCompat;

import android.telephony.SmsMessage;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import edu.cmu.pocketsphinx.Assets;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.SpeechRecognizer;
import edu.cmu.pocketsphinx.SpeechRecognizerSetup;


import static android.widget.Toast.makeText;


import android.widget.ToggleButton;

//TODO разобраться с чекбоксом

public class PocketSphinxActivity extends Activity implements
        RecognitionListener {

    public static final int STARTUP_DELAY = 300;
    public static final int ANIM_ITEM_DURATION = 1500;
    public static final int ITEM_DELAY = 300;
    private boolean animationStarted = false;


    private static final String TAG = "myLogs";

    /* Named searches allow to quickly reconfigure the decoder */
    private static final String KWS_SEARCH = "wakeup";
    private static final String FORECAST_SEARCH = "озвуч";
    private static String DIGITS_SEARCH = "сообщение";

    /** добавить ещё одну константу приравнивающую  себе MessageKey из isCreateStringContacts*/

    private static final String PHONE_SEARCH = "";
    private static final String MENU_SEARCH = "меню";

    public static String MESSAGE_SEARCH="светка";

    /* Keyword we are looking for to activate menu */
    private static final String KEYPHRASE = "андроид";

    /* Used to handle permission request */
    private static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;

    private SpeechRecognizer recognizer;
    private HashMap<String, Integer> captions;


    /** Для класса Speaker  */
    private final int CHECK_CODE = 0x1;
    private final int LONG_DURATION = 5000;
    private final int SHORT_DURATION = 1200;

    private Speaker speaker;

    public static ArrayMap<String, String> PersonMessage;
    private ToggleButton toggle;
    private CompoundButton.OnCheckedChangeListener toggleListener;

    private BroadcastReceiver smsReceiver;

    public int mStreamID;
    public SoundPool soundPool;
    public SoundPlayback soundPlayback;

    public int soundIdreadiness;
    public int soundIdactivation;
    public AssetManager mAssetManager;
    public static boolean soundOn= true;
    public ContentResolver cr;//если не будет работать убрать это
    public Message mess;//если не будет работать убрать это
    @Override
    public void onCreate(Bundle state) {
        setTheme(R.style.AppTheme);
        super.onCreate(state);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        soundPlayback=new SoundPlayback();
        cr=getContentResolver();//если не будет работать убрать это
        mess=new Message();//если не будет работать убрать это
        mAssetManager = getAssets();

        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP ) {
            soundPlayback.createOldSoundPool();
        }
        else {
            // SoundPool(int maxStreams, int streamType, int srcQuality)
            soundPlayback.createNewSoundPool();
        }

        soundIdactivation=soundPlayback.loadSound("activation.mp3",mAssetManager);
        soundIdreadiness= soundPlayback.loadSound("readiness.wav",mAssetManager);

       /** надо попробовать убрать эти строки*/
     //   ContentResolver cr =getContentResolver();
     //   Message mess= new Message();

        String nameMessage=isCreateStringContacts(cr,mess);
        PocketSphinxActivity.MESSAGE_SEARCH=nameMessage.substring(0,nameMessage.length()-2);


    // Prepare the data for UI
        captions = new HashMap<String, Integer>();
        captions.put(KWS_SEARCH, R.string.kws_caption);
        captions.put(MENU_SEARCH, R.string.menu_caption);
        captions.put(DIGITS_SEARCH, R.string.digits_caption);
        captions.put(FORECAST_SEARCH, R.string.forecast_caption);

        setContentView(R.layout.main);

        int permissionCheck = ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECORD_AUDIO);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSIONS_REQUEST_RECORD_AUDIO);
            return;
        }

        CheckBox sounCheckBox = (CheckBox) findViewById(R.id.sounCheckBox);

        sounCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    PocketSphinxActivity.soundOn=true;
                    soundPlayback.playSound(soundIdactivation);
                }
                else {
                   PocketSphinxActivity.soundOn=false;

                }
            }
        });

        /** при нажатии запускает бота и унижтажает его при повторном нажатии*/
        toggle = (ToggleButton)findViewById(R.id.speechToggle);
        toggleListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton view, boolean isChecked) {
                if(isChecked){
                    ((TextView) findViewById(R.id.caption_text)).setText(R.string.preparation_bot);
                    checkTTS();
                    runRecognizerSetup();
                    initializeSMSReceiver();
                    registerSMSReceiver();
                }else{
                    ((TextView) findViewById(R.id.caption_text)).setText(R.string.stop_bot);
                    unregisterReceiver(smsReceiver);
                    recognizer.cancel();
                    recognizer.shutdown();
            }
            }
        };
        toggle.setOnCheckedChangeListener(toggleListener);
        PatchFile(cr,mess);

    }

    /** ПОМЕНЯТЬ ТЕМУ У КНОПКИ ВЫХОДА И ТОГДА ЗАРАБОТАЕТ*/
    public void ExitOnclick(View view){
        finish();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        if (!hasFocus || animationStarted) {
            return;
        }
        animate();
        super.onWindowFocusChanged(hasFocus);
    }

    private void animate() {
        ImageView logoImageView = (ImageView) findViewById(R.id.img_logo);
        ViewGroup container = (ViewGroup) findViewById(R.id.container);

        ViewCompat.animate(logoImageView)
                .translationY(-250)
                .setStartDelay(STARTUP_DELAY)
                .setDuration(ANIM_ITEM_DURATION).setInterpolator(
                new DecelerateInterpolator(1.2f)).start();

        for (int i = 0; i < container.getChildCount(); i++) {
            View v = container.getChildAt(i);
            ViewPropertyAnimatorCompat viewAnimator;

            if (!(v instanceof Button)) {
                viewAnimator = ViewCompat.animate(v)
                        .translationY(50).alpha(1)
                        .setStartDelay((ITEM_DELAY * i) + 500)
                        .setDuration(1500);
            } else {
                viewAnimator = ViewCompat.animate(v)
                        .scaleY(1).scaleX(1)
                        .setStartDelay((ITEM_DELAY * i) + 500)
                        .setDuration(500);
            }

            viewAnimator.setInterpolator(new DecelerateInterpolator()).start();
        }
    }

    private void initializeSMSReceiver(){

        smsReceiver = new BroadcastReceiver(){
            @Override
            public void onReceive(Context context, Intent intent) {

                Bundle bundle = intent.getExtras();

                String text = null;
                String sender;
                if(bundle!=null){

                    Object[] pdus = (Object[])bundle.get("pdus");
                    for(int i=0;i<pdus.length;i++){
                        byte[] pdu = (byte[])pdus[i];
                        SmsMessage message = SmsMessage.createFromPdu(pdu);

                        text = message.getDisplayMessageBody();
                        sender = mess.getContactName(message.getOriginatingAddress(),cr);

                        speaker.speak("Пришло Сообщение от абонента"+sender+ "!");
                    }
                    setResultData(text);
                }
            }
        };
    }

    private void checkTTS(){
        Intent check = new Intent();
        check.setAction(TextToSpeech.Engine.ACTION_CHECK_TTS_DATA);
        startActivityForResult(check, CHECK_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == CHECK_CODE){
            if(resultCode == TextToSpeech.Engine.CHECK_VOICE_DATA_PASS){
                speaker = new Speaker(this);
            }else {
                Intent install = new Intent();
                install.setAction(TextToSpeech.Engine.ACTION_INSTALL_TTS_DATA);
                startActivity(install);
            }
        }
    }

  /*  private String getContactName(String phone){
        Uri uri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phone));
        String projection[] = new String[]{ContactsContract.Data.DISPLAY_NAME};
        Cursor cursor = getContentResolver().query(uri, projection, null, null, null);
        if(cursor.moveToFirst()){
            return cursor.getString(0);
        }else {
            return "unknown number";
        }
    }*/

    private void registerSMSReceiver() {
        IntentFilter intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        registerReceiver(smsReceiver, intentFilter);
    }

    public void PatchFile(ContentResolver cr,Message message){

        EntryFile entryfile=new EntryFile();
        String MessagePerson=isCreateStringContacts(cr,message);

        try {
            Assets assets = new Assets(PocketSphinxActivity.this);
            File assetDir = assets.syncAssets();
            entryfile.ReadLastLine(assetDir,MessagePerson);

        } catch (IOException e) {

        }
    }

    public  String isCreateStringContacts(ContentResolver cr,Message message){

        PocketSphinxActivity.PersonMessage = message.isGiveMessage(cr);
        String MessageKey="";

        for (String key : PersonMessage.keySet()) {
            if(key.matches("^[а-яё\" \"]+$")){
                MessageKey=MessageKey + " | "+ key ;
            }
        }
        MessageKey=MessageKey.substring(3);
        MessageKey= MessageKey +"; ";

        return MessageKey;
    }

    public void runRecognizerSetup() {
        new AsyncTask<Void, Void, Exception>() {
            @Override
            protected Exception doInBackground(Void... params) {
                try {
                    Assets assets = new Assets(PocketSphinxActivity.this);
                    File assetDir = assets.syncAssets();
                    setupRecognizer(assetDir);
                    recognizer.startListening(KWS_SEARCH);

                    soundPlayback.playSound(soundIdreadiness);

                } catch (IOException e) {
                    return e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Exception result) {
                if (result != null) {
                    ((TextView) findViewById(R.id.caption_text))
                            .setText(R.string.speach_not_found +" "+ result);
                } else {
                    switchSearch(KWS_SEARCH);

                }
            }
        }.execute();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSIONS_REQUEST_RECORD_AUDIO) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                runRecognizerSetup();
            } else {
                finish();
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (recognizer != null) {
            recognizer.cancel();
            recognizer.shutdown();
        }

        if(smsReceiver!=null){
            unregisterReceiver(smsReceiver);
        }
        if(speaker!=null){
            speaker.destroy();
        }
        soundPool.release();
        soundPool = null;
    }

    /**
     * In partial result we get quick updates about current hypothesis. In
     * keyword spotting mode we can react here, in other modes we need to wait
     * for final result in onResult.
     */
    @Override
    public void onPartialResult(Hypothesis hypothesis) {
        Message mess= new Message();
        ContentResolver cr =getContentResolver();
        String bodyMessage;

        if (hypothesis == null)
            return;

        String text = hypothesis.getHypstr();

        if (text.equals(KEYPHRASE)){
            switchSearch(MENU_SEARCH);
            soundPlayback.playSound(soundIdactivation);
          //  soundPlayback.playSound(soundIdreadiness);
        }

        else if (text.equals(FORECAST_SEARCH)){
            speaker.speak(smsReceiver.getResultData());
            switchSearch(FORECAST_SEARCH);
        }

        else if (PersonMessage.containsKey(text)){
            /** проверить на корректность */
            bodyMessage=mess.MessageOutput(text,cr);
            SpeachMessage(bodyMessage);
            switchSearch(FORECAST_SEARCH);
        }
        else{
            //((TextView) findViewById(R.id.result_text)).setText(text);
        }
    }

    public void SpeachMessage(String bodyMessage){
        if(bodyMessage==null){
            speaker.speak(getString(R.string.message_not_found));
        }else{
            speaker.speak(bodyMessage);
        }
    }

    /**
     * This callback is called when we stop the recognizer.
     */
    @Override
    public void onResult(Hypothesis hypothesis) {
        /** getContentResolver() в будущем надо убрать
         он нужен для Context Который не передаётся из класса Message*/

        //((TextView) findViewById(R.id.result_text)).setText("");
        if (hypothesis != null) {
            String text = hypothesis.getHypstr();

               // makeText(this, text, Toast.LENGTH_SHORT).show(); /** выводит результат распознавание в тостах */
        }
    }

    @Override
    public void onBeginningOfSpeech() {
    }

    /**
     * We stop recognizer here to get a final result
     */
    @Override
    public void onEndOfSpeech() {
        if (!recognizer.getSearchName().equals(KWS_SEARCH))
            switchSearch(KWS_SEARCH);
    }

    private void switchSearch(String searchName) {
        recognizer.stop();

        // If we are not spotting, start listening with timeout (10000 ms or 10 seconds).
        if (searchName.equals(KWS_SEARCH))
            recognizer.startListening(searchName);
        else
            recognizer.startListening(searchName, 10000);

        String caption = getResources().getString(captions.get(searchName));
        ((TextView) findViewById(R.id.caption_text)).setText(caption);
    }

    private void setupRecognizer(File assetsDir) throws IOException {

        recognizer = SpeechRecognizerSetup.defaultSetup()
                .setAcousticModel(new File(assetsDir, "ru-rus-ptm"))
                .setDictionary(new File(assetsDir, "ru-ru.dict"))
                .setRawLogDir(assetsDir) // To disable logging of raw audio comment out this call (takes a lot of space on the device)

                .setRawLogDir(assetsDir).setKeywordThreshold(1e-20f)
                .setFloat("-beam", 1e-30f)

                .getRecognizer();
        recognizer.addListener(this);

        /** In your application you might not need to add all those searches.
         * They are added here for demonstration. You can leave just one.
         */

        // Create keyword-activation search.
        recognizer.addKeyphraseSearch(KWS_SEARCH, KEYPHRASE);

        // Create grammar-based search for selection between demos
        File menuGrammar = new File(assetsDir, "menu.gram");
        recognizer.addGrammarSearch(MENU_SEARCH, menuGrammar);

        // Create language model search
        File languageModel = new File(assetsDir, "weather.dmp");
        recognizer.addNgramSearch(FORECAST_SEARCH, languageModel);

    }

    @Override
    public void onError(Exception error) {
        ((TextView) findViewById(R.id.caption_text)).setText(error.getMessage());
    }

    @Override
    public void onTimeout() {
        switchSearch(KWS_SEARCH);
    }

}


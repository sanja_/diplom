package edu.cmu.pocketsphinx.demo;


import android.util.Log;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public  class EntryFile extends PocketSphinxActivity {

  final String LOG_TAG = "myLogs";

/** */
    public  void ReadLastLine(File file,String MessagePerson) throws IOException {
        String result = null;
        File f = new File( file,"menu.gram");

        /** заменяю последнюю строку на строку имен отправителей сообщений*/

        PrintWriter prWr = new PrintWriter(new BufferedWriter(new FileWriter(f, false)));

        result= "#JSGF V1.0;"+"\n"+"grammar menu;"+"\n"+"public <item> = озвуч | "+MessagePerson ;

        Log.d(LOG_TAG,"qqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq"+result);

        /**  перезаписываю файл с новой строкой имен отправителей сообщений (всё это должно производиться до инициализации слашателя PocketSpinx) */

        prWr.write(result);

        prWr.close();
    }
}
